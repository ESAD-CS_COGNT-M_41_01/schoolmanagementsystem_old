﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class ClassRoutine
    {
        public int Id { get; set; }
        [Required]
        public int DayOfWeek { get; set; }
        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:hh\\:mm}")]
        public Nullable<System.TimeSpan> StartTime { get; set; }
        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:hh\\:mm}")]
        public Nullable<TimeSpan> EndTime { get; set; }
        [Required]
        [StringLength(100)]
        public string ClassDueation { get; set; }
        [Required]
        public Nullable<int> PeriodNumber { get; set; }

        //[Required]
        //public int SubjectId { get; set; }
        //[Required]
        //public int TeacherId { get; set; }
        //[Required]
        //public int ClassRoomId { get; set; }
        public virtual Subject Subject { get; set; }
        public virtual Teacher Teacher { get; set; }
        public virtual ClassRoom ClassRoom { get; set; }

    }
}
