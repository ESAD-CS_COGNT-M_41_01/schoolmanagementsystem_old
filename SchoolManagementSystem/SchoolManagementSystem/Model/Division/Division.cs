﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class Division
    {
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string DivisionName { get; set; }
        //[Required]
        //public int CountryId { get; set; }
        public virtual Country Country { get; set; }
        public virtual ICollection<AdmissionApply> AdmissionApply { get; set; }
        public virtual ICollection<District> Districts { get; set; }
        public virtual ICollection<Guardian> Guardians { get; set; }
        public virtual ICollection<Staff> Staffs { get; set; }
        public virtual ICollection<Student> Students { get; set; }
        public virtual ICollection<Teacher> Teachers { get; set; }
    }
}
