﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class Subject
    {  [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string SubjectName { get; set; }
        [Required]
        [StringLength(6)]
        public string SubjectCode { get; set; }

        [Required]
        public int SubjectGroupId { get; set; }

        public virtual SubjectGroup SubjectGroup { get; set; }
        public virtual ICollection<ClassRoutine> ClassRoutine { get; set; }
        public virtual ICollection<ExamMarks> ExamMarks { get; set; }
        public virtual ICollection<ExamRoutine> ExamRoutine { get; set; }
        public virtual ICollection<Teacher> Teachers { get; set; }
    }
}
