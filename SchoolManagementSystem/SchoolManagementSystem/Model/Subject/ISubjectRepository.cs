﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public interface ISubjectRepository
    {
        IEnumerable<Subject> GetAll();
        Subject Find(int id);
        int Remove(int id);
        Task<int> SaveSubject(Subject subject);
    }
}
