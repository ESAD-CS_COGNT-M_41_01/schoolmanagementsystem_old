﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class SubjectRepository : ISubjectRepository
    {

        private readonly ApplicationDbContext _context;
        public SubjectRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public Subject Find(int id)
        {
            if (SubjectExists(id))
            {
                return this.GetAll().Where(s => s.Id == id).FirstOrDefault();
            }
            return null;
        }

        public IEnumerable<Subject> GetAll()
        {
            return _context.Subject.ToList();
        }

        public int Remove(int id)
        {
            Subject s = _context.Subject.Find(id);
            if (s != null)
            {
                _context.Subject.Remove(s);
            }
            try
            {
                var res = _context.SaveChanges();
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Task<int> SaveSubject(Subject subject)
        {
            if (subject.Id == 0)
            {
                _context.Subject.Add(subject);
            }
            else
            {
                Subject s = _context.Subject.Find(subject.Id);

                if (s != null)
                {
                    //Update that Subject
                    s.SubjectName = subject.SubjectName;
                    s.SubjectCode = subject.SubjectCode;
                    s.SubjectGroupId = subject.SubjectGroupId;

                    _context.Subject.Update(s);
                }
            }
            try
            {
                var result = _context.SaveChangesAsync();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool SubjectExists(int id)
        {
            return _context.Subject.Any(se => se.Id == id);
        }
    }
}
