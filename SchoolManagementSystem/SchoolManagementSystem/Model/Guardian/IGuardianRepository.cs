﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public interface IGuardianRepository
    {
        void Add(Guardian guardian);
        IEnumerable<Guardian> GetAll();
        Guardian Find(int id);
        Guardian Remove(int id);
        void Update(Guardian guardian);
    }
}