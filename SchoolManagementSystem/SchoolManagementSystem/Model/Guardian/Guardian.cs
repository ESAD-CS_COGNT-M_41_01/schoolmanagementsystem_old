﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class Guardian : PersonInfo
    {
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string AltGuardianName { get; set; }
        [Required]
        [StringLength(100)]
        public string RelationOfAltGuardian { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Invalid Contact Number Format")]
        [RegularExpression(@"\880[0-9]{11}+", ErrorMessage = "Invalid Contact Number Format")]
        public string AltGuardianPhoneNo { get; set; }
        [Required]
        [DataType(DataType.EmailAddress, ErrorMessage = "Invalid Email Address")]
        public string AltGuardianEmail { get; set; }
        public virtual ICollection<Student> Students { get; set; }
    }
}
