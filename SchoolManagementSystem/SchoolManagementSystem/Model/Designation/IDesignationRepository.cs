﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem
{
    public interface IDesignationRepository
    {
        IEnumerable<Designation> Designations { get; }
        Task<int> SaveDesignation(Designation designation);
        Task<Designation> DeleteDesignation(int? id);
    }
}
