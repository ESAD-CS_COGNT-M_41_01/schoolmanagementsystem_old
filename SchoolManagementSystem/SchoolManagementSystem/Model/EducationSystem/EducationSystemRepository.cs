﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class EducationSystemRepository : IEducationSystemRepository
    {
        private readonly ApplicationDbContext _context;
        public EducationSystemRepository(ApplicationDbContext context)
        {
            _context = context;
        }     
        public IEnumerable<EducationSystem> GetAll()
        {
            return _context.EducationSystem.ToList();          
        }
        public EducationSystem Find(int id)
        {
            if(EducationSystemExists(id))
            {
                return this.GetAll().Where(edu => edu.Id == id).FirstOrDefault();
            }
            return null;
        }
        public Task<int> SaveEducationSystem(EducationSystem educationSystem)
        {
            if (educationSystem.Id == 0)
            {
                _context.EducationSystem.Add(educationSystem);
            }
            else
            {
                EducationSystem edu = _context.EducationSystem.Find(educationSystem.Id);

                if (edu != null)
                {
                    //Update that educationSystem
                    edu.Name = educationSystem.Name;
                    edu.StudentType = educationSystem.StudentType;
                    edu.SchoolVersionId = educationSystem.SchoolVersionId;
                    edu.ShiftId = educationSystem.ShiftId;
                    edu.BrunchId = educationSystem.BrunchId;
                    _context.EducationSystem.Update(edu);
                }
            }
            try
            {
                var result = _context.SaveChangesAsync();
                return result;
            }
            catch(Exception ex)
            {
                throw ex;
            }        
        }
        public int Remove(int id)
        {
            EducationSystem edu = _context.EducationSystem.Find(id);
            if (edu != null)
            {
                _context.EducationSystem.Remove(edu);
            }
            try
            {                
               var res =   _context.SaveChanges();
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool EducationSystemExists(int id)
        {
            return _context.EducationSystem.Any(e => e.Id == id);
        }
    }
}
