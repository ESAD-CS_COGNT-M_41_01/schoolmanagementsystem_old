﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class EducationSystem
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [Required]
        public int StudentType { get; set; }
        [Required]
        public int BrunchId { get; set; }
        [Required]
        public int SchoolVersionId { get; set; }
        [Required]
        public int ShiftId { get; set; }
        public virtual Brunch Brunch { get; set; }
        public virtual SchoolVersion SchoolVersion { get; set; }
        public virtual Shift Shift { get; set; }
    }
}
