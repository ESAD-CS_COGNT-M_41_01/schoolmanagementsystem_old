﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public interface IEducationSystemRepository
    {
        IEnumerable<EducationSystem> GetAll();
        EducationSystem Find(int id);
        int Remove(int id);
        Task<int> SaveEducationSystem(EducationSystem educationSystem);
    }
}
