﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class ExamMarks
    {
        public int Id { get; set; }

        //[Required]
        //public int ExamId { get; set; }
        //[Required]
        //public int SubjectId { get; set; }
        //[Required]
        //public int StudentId { get; set; }

        public virtual Exam Exam { get; set; }
        public virtual Subject Subject { get; set; }
        public virtual Student Student { get; set; }
        public int TotalMark { get; set; }
        public int ObtainMark { get; set; }

    }
}
