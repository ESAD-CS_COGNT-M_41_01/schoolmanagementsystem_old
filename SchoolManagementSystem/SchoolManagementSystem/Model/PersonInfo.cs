﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolManagementSystem.Model
{
    public class PersonInfo
    {
        [Required]
        [StringLength(100)]
        public string FirstName { get; set; }        
        [Required]
        [StringLength(100)]
        public string LastName { get; set; }
        public string FullName { get { return FirstName + " " + LastName; } }
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }
        [Required]
        [StringLength(8)]
        [DataType(DataType.Password)]
        [NotMapped]
        public string Password { get; set; }
        [Required(ErrorMessage = "Password not match")]
        [DataType(DataType.Password)]
        [NotMapped]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
        [StringLength(12)]
        public string PasswordHint { get; set; }
        [Required]
        public int Gender { get; set; }
        [Required]
        public int Religion { get; set; }
        [Required]
        [StringLength(15)]
        public string PhoneNo { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [StringLength(30)]
        public string NidNo { get; set; }        
        public string ImageUrl { get; set; }
        [StringLength(500)]
        public string CurrentStatus { get; set; }
        [Required]
        public DateTime CreatedDate { get {return DateTime.Now; } }
        [Required]
        [StringLength(500)]
        public string PresentAddress { get; set; }
        [Required]
        [StringLength(500)]
        public string ParmanentAddress { get; set; }


        //[Required]
        //public int CountryId { get; set; }
        //[Required]
        //public int DivisionId { get; set; }
        //[Required]
        //public int DistrictId { get; set; }
        //[Required]
        //public int PoliceStationId { get; set; }
        //[Required]
        //public int PostOfficeId { get; set; }

        public virtual Country Country { get; set; }
        public virtual District District { get; set; }
        public virtual Division Division { get; set; }
        public virtual PoliceStation PoliceStation { get; set; }
        public virtual PostOffice PostOffice { get; set; }
        
    }
}