﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class Holiday
    {
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string HolidayName { get; set; }
        [Required]
        public Nullable<System.DateTime> StartDate { get; set; }
        [Required]
        public Nullable<System.DateTime> EndDate { get; set; }
        [Required]
        public Nullable<int> NumberOfDay { get; set; }
        //[Required]
        //public int BrunchId { get; set; }
        public virtual Brunch Brunch { get; set; }
    }
}
