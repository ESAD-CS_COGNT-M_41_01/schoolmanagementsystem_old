﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem
{
    public interface IBrunchRepository
    {
        public Task<IEnumerable<Brunch>> GetBrunchs();
        public Task<Brunch> GetBrunch(int id);
        Task<int> SaveBrunch(Brunch brunch);
        Task<Brunch> DeleteBrunch(int? id);
    }
}
