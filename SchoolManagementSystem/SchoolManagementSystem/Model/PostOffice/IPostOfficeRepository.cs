﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
   public interface IPostOfficeRepository
    {
        IEnumerable<PostOffice> GetAll();
        PostOffice Find(int id);
        int Remove(int id);
        Task<int> SavePostOffice(PostOffice postOffice);
    }
}
