﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public interface ISchoolClassRepository
    {
        void Add(SchoolClass schoolClass);
        IEnumerable<SchoolClass> GetAll();
        SchoolClass Find(int id);
        SchoolClass Remove(int id);
        void Update(SchoolClass schoolClass);
    }
}
