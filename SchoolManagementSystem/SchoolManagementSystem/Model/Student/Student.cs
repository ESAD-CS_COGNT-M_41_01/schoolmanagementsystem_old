﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class Student : PersonInfo
    {
        public int Id { get; set; }        
        public int RegistrationNo { get; set; }
        [Required]        
        public int RollNo { get; set; }
        [Required]
        [StringLength(100)]
        public string FatherName { get; set; }
        [Required]
        [StringLength(100)]
        public string FatherOccupation { get; set; }
        [Required]
        [StringLength(15)]
        public string FatherPhoneNo { get; set; }
        [Required]
        [StringLength(100)]
        public string MotherName { get; set; }
        [Required]
        [StringLength(100)]
        public string MotherOccupation { get; set; }
        [Required]
        [StringLength(15)]
        public string MotherPhone { get; set; }
        public string FormarSchoolName { get; set; }
        [Required]
        public DateTime AdmissionDate { get; set; }

        //[Required]
        //public int SchoolClassId { get; set; }
        //[Required]       
        //public int QuotaId { get; set; }
        //[Required]        
        //public int GuardianId { get; set; }
        //[Required]
        //public int BrunchId { get; set; }
        public virtual Brunch Brunch { get; set; }
        public virtual SchoolClass SchoolClass { get; set; }
        public virtual Quota Quota { get; set; }
        public virtual Guardian Guardian { get; set; }
        public virtual ICollection<ExamResult> ExamResult { get; set; }
        public virtual ICollection<ExamMarks> ExamMarks { get; set; }

    }
}
