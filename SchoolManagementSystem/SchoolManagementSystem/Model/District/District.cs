﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class District
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string DistrictName { get; set; }
        //[Required]
        //public int DivisionId { get; set; }
        public virtual ICollection<AdmissionApply> AdmissionApply { get; set; }
        public virtual Division Division { get; set; }
        public virtual ICollection<Guardian> Guardians { get; set; }
        public virtual ICollection<PoliceStation> PoliceStation { get; set; }
        public virtual ICollection<Staff> Staffs { get; set; }
        public virtual ICollection<Student> Students { get; set; }
        public virtual ICollection<Teacher> Teachers { get; set; }
    }
}
