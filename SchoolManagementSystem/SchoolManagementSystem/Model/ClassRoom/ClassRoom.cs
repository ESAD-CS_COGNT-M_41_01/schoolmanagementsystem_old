﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class ClassRoom
    {
        public int Id { get; set; }

        [Required]
        public int SchoolClassId { get; set; }
        [Required]
        public int SectionId { get; set; }
        [Required]
        public int RoomId { get; set; }
        [Required]
        public int ShiftId { get; set; }
        public virtual SchoolClass SchoolClass { get; set; }
        public virtual Room Room { get; set; }
        public virtual Section Section { get; set; }
        public virtual Shift Shift { get; set; }    
        public virtual ICollection<ClassRoutine> ClassRoutine { get; set; }        
    }
}
