﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class ClassRoomRepository : IClassRoomRepository
    {
        ApplicationDbContext _context;
        public ClassRoomRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<ClassRoom>> GetClassRooms()
        {
            if (_context != null)
            {
                return await _context.ClassRoom.ToListAsync();
            }
            return null;
        }

        public async Task<ClassRoom> GetClassRoom(int id)
        {
            if (ClassRoomExists(id))
            {
                var classRoom = await _context.ClassRoom.FindAsync(id);
                return classRoom;
            }
            return null;

        }
        public async Task<ClassRoom> CreateClassRoom(ClassRoom classRoom)
        {
            if (classRoom.Id == 0)
            {
                _context.ClassRoom.Add(classRoom);
            }
            try
            {
                await _context.SaveChangesAsync();
                return classRoom;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<ClassRoom> UpdateClassRoom(int id, ClassRoom classRoom)
        {
            ClassRoom clr = _context.ClassRoom.Find(classRoom.Id);

            if (clr != null)
            {
                //Update that educationSystem
                clr.SchoolClassId = classRoom.SchoolClassId;
                clr.SectionId = classRoom.SectionId;
                clr.RoomId = classRoom.RoomId;
                clr.ShiftId = classRoom.ShiftId;

                _context.ClassRoom.Update(clr);
            }
            try
            {
                var result = await _context.SaveChangesAsync();
                return classRoom;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<ClassRoom> DeleteClassRoom(int id)
        {
            var clr = await _context.ClassRoom.FindAsync(id);
            if (clr != null)
            {
                _context.ClassRoom.Remove(clr);
            }
            try
            {
                var res = await _context.SaveChangesAsync();
                return clr;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private bool ClassRoomExists(int id)
        {
            return _context.ClassRoom.Any(e => e.Id == id);
        }
    }
}
