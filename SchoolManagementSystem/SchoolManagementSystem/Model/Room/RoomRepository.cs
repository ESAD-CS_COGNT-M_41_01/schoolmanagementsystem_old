﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SchoolManagementSystem.Model;


namespace SchoolManagementSystem
{
    public class RoomRepository : IRoomRepository
    {
        public Task<ClassRoom> CreateRoom(Room room)
        {
            throw new NotImplementedException();
        }

        public Task<ClassRoom> DeleteRoom(int id)
        {
            throw new NotImplementedException();
        }

        public Task<Room> GetRoom(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Room>> GetRooms()
        {
            throw new NotImplementedException();
        }

        public Task<ClassRoom> UpdateRoom(int id, Room room)
        {
            throw new NotImplementedException();
        }

        Task<Room> IRoomRepository.CreateRoom(Room room)
        {
            throw new NotImplementedException();
        }

        Task<Room> IRoomRepository.DeleteRoom(int id)
        {
            throw new NotImplementedException();
        }

        Task<Room> IRoomRepository.UpdateRoom(int id, Room room)
        {
            throw new NotImplementedException();
        }
    }
}
