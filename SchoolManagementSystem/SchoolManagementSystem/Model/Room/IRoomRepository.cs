﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem
{
    public interface IRoomRepository
    {
        public Task<IEnumerable<Room>> GetRooms();
        public Task<Room> GetRoom(int id);
        public Task<Room> UpdateRoom(int id, Room room);
        public Task<Room> CreateRoom(Room room);
        public Task<Room> DeleteRoom(int id);

    }
}
