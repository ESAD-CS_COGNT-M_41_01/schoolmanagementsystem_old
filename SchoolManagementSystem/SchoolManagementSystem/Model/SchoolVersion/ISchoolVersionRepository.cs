﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public interface ISchoolVersionRepository
    {
        void Add(SchoolVersion schoolVersion);
        IEnumerable<SchoolVersion> GetAll();
        SchoolVersion Find(int id);
        SchoolVersion Remove(int id);
        void Update(SchoolVersion schoolClass);
    }
}
