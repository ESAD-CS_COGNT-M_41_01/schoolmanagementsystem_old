﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class SchoolVersion
    {
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string SchoolVersionName { get; set; }
        [Required]
        [StringLength(100)]
        public string BookType { get; set; }

        public virtual ICollection<EducationSystem> EducationSystem { get; set; }
    }
}
