﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class NoticeBoard
    {
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string TopicName { get; set; }
        [Required]
        [StringLength(1000)]
        public string NoticeBody { get; set; }
        [Required]
        public Nullable<System.DateTime> PublishDate { get; set; }
        //[Required]
        //public int BrunchId { get; set; }
        //[Required]
        //public int SchoolClassId { get; set; }
        public virtual Brunch Brunch { get; set; }
        public virtual SchoolClass SchoolClass { get; set; }
    }
}
