﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public interface ISubjectGroupRepository
    {
        IEnumerable<SubjectGroup> GetAll();
        SubjectGroup Find(int id);
        int Remove(int id);
        Task<int> SaveSubjectGroup(SubjectGroup subjectGroup);
    }
}

