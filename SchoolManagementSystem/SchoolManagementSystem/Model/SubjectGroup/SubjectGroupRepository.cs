﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class SubjectGroupRepository : ISubjectGroupRepository
    {
        ApplicationDbContext _context;
        public SubjectGroupRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public SubjectGroup Find(int id)
        {
            if (SubjectGroupExists(id))
            {
                return this.GetAll().Where(sg => sg.Id == id).FirstOrDefault();
            }
            return null;
        }

        public IEnumerable<SubjectGroup> GetAll()
        {
            return _context.SubjectGroup.ToList();
        }

        public int Remove(int id)
        {
            SubjectGroup sg = _context.SubjectGroup.Find(id);
            if (sg != null)
            {
                _context.SubjectGroup.Remove(sg);
            }
            try
            {
                var res = _context.SaveChanges();
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Task<int> SaveSubjectGroup(SubjectGroup subjectGroup)
        {
            if (subjectGroup.Id == 0)
            {
                _context.SubjectGroup.Add(subjectGroup);
            }
            else
            {
                SubjectGroup sg = _context.SubjectGroup.Find(subjectGroup.Id);

                if (sg != null)
                {
                    //Update that SubjectGroup
                    sg.GroupName = subjectGroup.GroupName;

                    _context.SubjectGroup.Update(sg);
                }
            }
            try
            {
                var result = _context.SaveChangesAsync();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool SubjectGroupExists(int id)
        {
            return _context.SubjectGroup.Any(e => e.Id == id);
        }
    }
}
