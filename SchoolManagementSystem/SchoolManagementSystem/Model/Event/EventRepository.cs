﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem
{
    public class EventRepository : IEventRepository
    {
        public Task<Event> CreateEvent(Event eve)
        {
            throw new NotImplementedException();
        }

        public Task<Event> DeleteEvent(int id)
        {
            throw new NotImplementedException();
        }

        public Task<Event> GetEvent(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Event>> GetEvents()
        {
            throw new NotImplementedException();
        }

        public Task<Event> UpdateEvent(int id, Event eve)
        {
            throw new NotImplementedException();
        }
    }
}
