﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{

    public class ExamResult
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public Nullable<System.DateTime> ResultPublishDate { get; set; }
        [Required]
        public int TotalScore { get; set; }
        [Required]
        public int HighScore { get; set; }        
        [Required]
        public int GrandTotalMarks { get; set; }
        [Required]
        public bool ResultStatus { get; set; }
        [Required]
        public double GradePoint { get; set; }
        public int Position { get; set; }        
        public int TotalPresent { get; set; }
        public int TotalAbsent { get; set; }
        [StringLength(400)]
        public string Note { get; set; }
        //[Required]
        //public int SchoolClassId { get; set; }
        //[Required]
        //public int ExamId { get; set; }
        //[Required]
        //public int StudentId { get; set; }

        public virtual SchoolClass SchoolClass { get; set; }
        public virtual Exam Exam { get; set; }
        public virtual Student Student { get; set; }

    }
    
}
