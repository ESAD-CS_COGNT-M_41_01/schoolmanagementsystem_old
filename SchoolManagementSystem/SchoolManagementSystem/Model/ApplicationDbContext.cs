﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SchoolManagementSystem.Identity;

namespace SchoolManagementSystem.Model
{


    public class ApplicationDbContext :  IdentityDbContext<ApplicationUser, ApplicationRole, string, IdentityUserClaim<string>, ApplicationUserRole, IdentityUserLogin<string>, IdentityRoleClaim<string>, IdentityUserToken<string>>
    {


        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        //Dedails About Under Method-https://entityframework.net/knowledge-base/39798317/identityuserlogin-string---requires-a-primary-key-to-be-defined-error-while-adding-migration
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ApplicationUserRole>(userRole =>
            {
                userRole.HasKey(ur => new { ur.UserId, ur.RoleId });

                userRole.HasOne(ur => ur.Role)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();

                userRole.HasOne(ur => ur.User)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();
            });
        }

        public virtual DbSet<AdmissionApply> AdmissionApply { get; set; }
        public virtual DbSet<Brunch> Brunch { get; set; }
        public virtual DbSet<SchoolClass> SchoolClass { get; set; }
        public virtual DbSet<ClassRoom> ClassRoom { get; set; }
        public virtual DbSet<ClassRoutine> ClassRoutine { get; set; }
        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<Designation> Designation { get; set; }
        public virtual DbSet<District> District { get; set; }
        public virtual DbSet<Division> Division { get; set; }
        public virtual DbSet<EducationSystem> EducationSystem { get; set; }
        public virtual DbSet<Event> Event { get; set; }
        public virtual DbSet<Exam> Exam { get; set; }
        public virtual DbSet<ExamResult> ExamResult { get; set; }
        public virtual DbSet<ExamRoutine> ExamRoutine { get; set; }
        public virtual DbSet<Guardian> Guardian { get; set; }
        public virtual DbSet<Holiday> Holiday { get; set; }
        public virtual DbSet<NoticeBoard> NoticeBoard { get; set; }
        public virtual DbSet<PoliceStation> PoliceStation { get; set; }
        public virtual DbSet<PostOffice> PostOffice { get; set; }
        public virtual DbSet<Quota> Quota { get; set; }
        public virtual DbSet<Room> Room { get; set; }
        public virtual DbSet<RulesRegulations> RulesRegulations { get; set; }
        public virtual DbSet<SchoolVersion> SchoolVersion { get; set; }
        public virtual DbSet<Section> Section { get; set; }
        public virtual DbSet<Shift> Shift { get; set; }
        public virtual DbSet<Staff> Staff { get; set; }
        public virtual DbSet<Student> Students { get; set; }
        public virtual DbSet<SubjectGroup> SubjectGroup { get; set; }
        public virtual DbSet<Subject> Subject { get; set; }
        public virtual DbSet<Teacher> Teacher { get; set; }
        public virtual DbSet<Worker> Worker { get; set; }

        public virtual DbSet<ApplicationRole> ApplicationRole { get; set; }

        public virtual DbSet<ApplicationUserRole> ApplicationUserRole { get; set; }
    }
}
