﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public interface ISectionRepository
    {
        void Add(Section section);
        IEnumerable<Section> GetAll();
        Section Find(int id);
        Section Remove(int id);
        void Update(Section section);
    }
}
