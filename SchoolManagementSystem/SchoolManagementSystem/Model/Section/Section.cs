﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class Section
    {
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string SectionName { get; set; }
        //[Required]
        //public int? SubjectGroupId { get; set; }
        //[Required]
        //public int? SchoolClassId { get; set; }
        public virtual ICollection<ClassRoom> ClassRoom { get; set; }
        public virtual SubjectGroup SubjectGroup { get; set; }
        public virtual SchoolClass SchoolClass { get; set; }
    }
}
