﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public  interface IRulesRegulationsRepository
    {
        IEnumerable<RulesRegulations> GetAll();
        RulesRegulations Find(int id);
        int Remove(int id);
        Task<int> SaveRulesRegulations(RulesRegulations rulesRegulations);
    }
}
