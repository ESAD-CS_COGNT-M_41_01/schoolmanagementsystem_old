﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class RulesRegulations
    {
        public int Id { get; set; }
        [Required]
        [StringLength(1000)]
        public string RuleDetails { get; set; }
        //[Required]
        //public int BrunchId { get; set; }
        public virtual Brunch Brunch { get; set; }
    }
}
