﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class Shift
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string ShiftName { get; set; }
        public virtual ICollection<ClassRoom> ClassRoom { get; set; }
        public virtual ICollection<EducationSystem> EducationSystem { get; set; }
        public virtual ICollection<ExamRoutine> ExamRoutine { get; set; }
    }
}
