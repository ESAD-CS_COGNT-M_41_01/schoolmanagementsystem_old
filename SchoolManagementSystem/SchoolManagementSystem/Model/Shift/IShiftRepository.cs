﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem
{

    public interface IShiftRepository
    {
        void Add(Shift shift);
        IEnumerable<Shift> GetAll();
        Shift Find(int id);
        Shift Remove(int id);
        void Update(Shift shift);
    }

}
