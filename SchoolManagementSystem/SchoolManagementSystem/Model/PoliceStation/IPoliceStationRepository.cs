﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
   public interface IPoliceStationRepository
    {
        IEnumerable<PoliceStation> GetAll();
        PoliceStation Find(int id);
        int Remove(int id);
        Task<int> SavePoliceStation(PoliceStation policeStation);
    }
}
