﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class AdmissionApply : PersonInfo
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string FatherName { get; set; }
        [Required]
        [StringLength(50)]
        public string FatherOccupation { get; set; }
        [Required]
        [MaxLength(15)]
        [MinLength(11)]
        public string FatherPhone { get; set; }

        [Required]
        [StringLength(50)]
        public string MotherName { get; set; }

        [Required]
        [StringLength(50)]
        public string MotherOccupation { get; set; }

        [Required(ErrorMessage = "Enter your mother's phone")]
        [MaxLength(15)]
        [MinLength(11)]
        public string MotherPhone { get; set; }

        [StringLength(30)]
        public string FormarSchoolName { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ApplingDate { get; set; }
        //[Required]
        //public int? SchoolClassId { get; set; }
        //[Required]
        //public int? QuotaId { get; set; }
        //[Required]
        //public int? BrunchId { get; set; }
        public virtual Brunch Brunch { get; set; }
        public virtual SchoolClass SchoolClass { get; set; }
        public virtual Quota Quota { get; set; }
    }
}
