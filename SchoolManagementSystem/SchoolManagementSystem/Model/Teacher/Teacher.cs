﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Model
{
    public class Teacher : PersonInfo
    {
        public int Id { get; set; }
        [Required]
        public bool MarritialStatus { get; set; }
        [Required]
        [StringLength(100)]
        public string FathersName { get; set; }
        [Required]
        [StringLength(100)]
        public string MothersName { get; set; }
        [Required]
        public DateTime joiningDate { get; set; }
        public Nullable<DateTime> ResignDate { get; set; }

        //[Required]       
        //public int BrunchId { get; set; }
        //[Required]
        //public int DesignationId { get; set; }
        //[Required]
        //public int SubjectId { get; set; }

        public virtual Brunch Brunch { get; set; }       
        public virtual ICollection<ClassRoutine> ClassRoutine { get; set; }
        public virtual Designation Designation { get; set; }
        public virtual Subject Subject { get; set; }
    }
}
