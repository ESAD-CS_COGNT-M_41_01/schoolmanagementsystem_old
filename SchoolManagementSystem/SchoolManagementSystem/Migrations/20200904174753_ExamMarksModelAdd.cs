﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SchoolManagementSystem.Migrations
{
    public partial class ExamMarksModelAdd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExamResult_Subject_SubjectId",
                table: "ExamResult");

            migrationBuilder.DropIndex(
                name: "IX_ExamResult_SubjectId",
                table: "ExamResult");

            migrationBuilder.DropColumn(
                name: "ExamPublishDate",
                table: "ExamResult");

            migrationBuilder.DropColumn(
                name: "GrandTotalScore",
                table: "ExamResult");

            migrationBuilder.DropColumn(
                name: "LatterGrade",
                table: "ExamResult");

            migrationBuilder.DropColumn(
                name: "ObtainScore",
                table: "ExamResult");

            migrationBuilder.DropColumn(
                name: "PassScore",
                table: "ExamResult");

            migrationBuilder.DropColumn(
                name: "SubjectId",
                table: "ExamResult");

            migrationBuilder.AddColumn<int>(
                name: "GrandTotalMarks",
                table: "ExamResult",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "ResultPublishDate",
                table: "ExamResult",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateTable(
                name: "ExamMarks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ExamId = table.Column<int>(nullable: true),
                    SubjectId = table.Column<int>(nullable: true),
                    StudentId = table.Column<int>(nullable: true),
                    TotalMark = table.Column<int>(nullable: false),
                    ObtainMark = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExamMarks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExamMarks_Exam_ExamId",
                        column: x => x.ExamId,
                        principalTable: "Exam",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExamMarks_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExamMarks_Subject_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subject",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExamMarks_ExamId",
                table: "ExamMarks",
                column: "ExamId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamMarks_StudentId",
                table: "ExamMarks",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamMarks_SubjectId",
                table: "ExamMarks",
                column: "SubjectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExamMarks");

            migrationBuilder.DropColumn(
                name: "GrandTotalMarks",
                table: "ExamResult");

            migrationBuilder.DropColumn(
                name: "ResultPublishDate",
                table: "ExamResult");

            migrationBuilder.AddColumn<DateTime>(
                name: "ExamPublishDate",
                table: "ExamResult",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "GrandTotalScore",
                table: "ExamResult",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "LatterGrade",
                table: "ExamResult",
                type: "nvarchar(10)",
                maxLength: 10,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "ObtainScore",
                table: "ExamResult",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PassScore",
                table: "ExamResult",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SubjectId",
                table: "ExamResult",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ExamResult_SubjectId",
                table: "ExamResult",
                column: "SubjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_ExamResult_Subject_SubjectId",
                table: "ExamResult",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
