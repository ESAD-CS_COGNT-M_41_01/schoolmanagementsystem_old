﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SchoolManagementSystem.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Brunch",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BrunchName = table.Column<string>(maxLength: 200, nullable: false),
                    Principal = table.Column<string>(maxLength: 100, nullable: false),
                    SchoolName = table.Column<string>(maxLength: 200, nullable: false),
                    SchoolAuthority = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Brunch", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Country",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    CountryCode = table.Column<string>(maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Country", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Designation",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DesignationName = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Designation", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Quota",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    QuotaName = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Quota", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SchoolClass",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ClassName = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SchoolClass", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SchoolVersion",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SchoolVersionName = table.Column<string>(maxLength: 100, nullable: false),
                    BookType = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SchoolVersion", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Shift",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ShiftName = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shift", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SubjectGroup",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GroupName = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubjectGroup", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Worker",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Worker", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Event",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EventName = table.Column<string>(maxLength: 200, nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    EventControlar = table.Column<string>(maxLength: 100, nullable: false),
                    ImageUrl = table.Column<string>(nullable: true),
                    BrunchId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Event", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Event_Brunch_BrunchId",
                        column: x => x.BrunchId,
                        principalTable: "Brunch",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Exam",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ExamType = table.Column<string>(maxLength: 100, nullable: false),
                    ExamDiscription = table.Column<string>(maxLength: 200, nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    Duration = table.Column<string>(maxLength: 100, nullable: false),
                    BrunchId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Exam", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Exam_Brunch_BrunchId",
                        column: x => x.BrunchId,
                        principalTable: "Brunch",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Holiday",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    HolidayName = table.Column<string>(maxLength: 100, nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    NumberOfDay = table.Column<int>(nullable: false),
                    BrunchId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Holiday", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Holiday_Brunch_BrunchId",
                        column: x => x.BrunchId,
                        principalTable: "Brunch",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Room",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoomName = table.Column<string>(maxLength: 10, nullable: false),
                    SitCapacity = table.Column<int>(nullable: false),
                    BrunchId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Room", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Room_Brunch_BrunchId",
                        column: x => x.BrunchId,
                        principalTable: "Brunch",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RulesRegulations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RuleDetails = table.Column<string>(maxLength: 1000, nullable: false),
                    BrunchId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RulesRegulations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RulesRegulations_Brunch_BrunchId",
                        column: x => x.BrunchId,
                        principalTable: "Brunch",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Division",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DivisionName = table.Column<string>(maxLength: 100, nullable: false),
                    CountryId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Division", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Division_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "NoticeBoard",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TopicName = table.Column<string>(maxLength: 100, nullable: false),
                    NoticeBody = table.Column<string>(maxLength: 1000, nullable: false),
                    PublishDate = table.Column<DateTime>(nullable: false),
                    BrunchId = table.Column<int>(nullable: true),
                    SchoolClassId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NoticeBoard", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NoticeBoard_Brunch_BrunchId",
                        column: x => x.BrunchId,
                        principalTable: "Brunch",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_NoticeBoard_SchoolClass_SchoolClassId",
                        column: x => x.SchoolClassId,
                        principalTable: "SchoolClass",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EducationSystem",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    StudentType = table.Column<int>(nullable: false),
                    BrunchId = table.Column<int>(nullable: true),
                    SchoolVersionId = table.Column<int>(nullable: true),
                    ShiftId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EducationSystem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EducationSystem_Brunch_BrunchId",
                        column: x => x.BrunchId,
                        principalTable: "Brunch",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EducationSystem_SchoolVersion_SchoolVersionId",
                        column: x => x.SchoolVersionId,
                        principalTable: "SchoolVersion",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EducationSystem_Shift_ShiftId",
                        column: x => x.ShiftId,
                        principalTable: "Shift",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Section",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SectionName = table.Column<string>(maxLength: 100, nullable: false),
                    SubjectGroupId = table.Column<int>(nullable: true),
                    SchoolClassId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Section", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Section_SchoolClass_SchoolClassId",
                        column: x => x.SchoolClassId,
                        principalTable: "SchoolClass",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Section_SubjectGroup_SubjectGroupId",
                        column: x => x.SubjectGroupId,
                        principalTable: "SubjectGroup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Subject",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SubjectName = table.Column<string>(maxLength: 100, nullable: false),
                    SubjectCode = table.Column<string>(maxLength: 6, nullable: false),
                    SubjectGroupId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subject", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Subject_SubjectGroup_SubjectGroupId",
                        column: x => x.SubjectGroupId,
                        principalTable: "SubjectGroup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "District",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DistrictName = table.Column<string>(maxLength: 100, nullable: true),
                    DivisionId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_District", x => x.Id);
                    table.ForeignKey(
                        name: "FK_District_Division_DivisionId",
                        column: x => x.DivisionId,
                        principalTable: "Division",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClassRoom",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SchoolClassId = table.Column<int>(nullable: true),
                    RoomId = table.Column<int>(nullable: true),
                    SectionId = table.Column<int>(nullable: true),
                    ShiftId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassRoom", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClassRoom_Room_RoomId",
                        column: x => x.RoomId,
                        principalTable: "Room",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClassRoom_SchoolClass_SchoolClassId",
                        column: x => x.SchoolClassId,
                        principalTable: "SchoolClass",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClassRoom_Section_SectionId",
                        column: x => x.SectionId,
                        principalTable: "Section",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClassRoom_Shift_ShiftId",
                        column: x => x.ShiftId,
                        principalTable: "Shift",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ExamRoutine",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ExamDate = table.Column<DateTime>(nullable: false),
                    ActiveStatus = table.Column<bool>(nullable: false),
                    SchoolClassId = table.Column<int>(nullable: true),
                    ExamId = table.Column<int>(nullable: true),
                    ShiftId = table.Column<int>(nullable: true),
                    SubjectId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExamRoutine", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExamRoutine_Exam_ExamId",
                        column: x => x.ExamId,
                        principalTable: "Exam",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExamRoutine_SchoolClass_SchoolClassId",
                        column: x => x.SchoolClassId,
                        principalTable: "SchoolClass",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExamRoutine_Shift_ShiftId",
                        column: x => x.ShiftId,
                        principalTable: "Shift",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExamRoutine_Subject_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subject",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PoliceStation",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PoliceStationName = table.Column<string>(maxLength: 100, nullable: false),
                    DistrictId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PoliceStation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PoliceStation_District_DistrictId",
                        column: x => x.DistrictId,
                        principalTable: "District",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PostOffice",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PostOfficeName = table.Column<string>(maxLength: 100, nullable: false),
                    PoliceStationId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostOffice", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PostOffice_PoliceStation_PoliceStationId",
                        column: x => x.PoliceStationId,
                        principalTable: "PoliceStation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AdmissionApply",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(maxLength: 100, nullable: false),
                    LastName = table.Column<string>(maxLength: 100, nullable: false),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    PasswordHint = table.Column<string>(maxLength: 12, nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    Religion = table.Column<int>(nullable: false),
                    PhoneNo = table.Column<string>(maxLength: 15, nullable: false),
                    Email = table.Column<string>(nullable: false),
                    NidNo = table.Column<string>(maxLength: 30, nullable: false),
                    ImageUrl = table.Column<string>(nullable: true),
                    CurrentStatus = table.Column<string>(maxLength: 500, nullable: true),
                    PresentAddress = table.Column<string>(maxLength: 500, nullable: false),
                    ParmanentAddress = table.Column<string>(maxLength: 500, nullable: false),
                    CountryId = table.Column<int>(nullable: true),
                    DistrictId = table.Column<int>(nullable: true),
                    DivisionId = table.Column<int>(nullable: true),
                    PoliceStationId = table.Column<int>(nullable: true),
                    PostOfficeId = table.Column<int>(nullable: true),
                    FatherName = table.Column<string>(maxLength: 50, nullable: false),
                    FatherOccupation = table.Column<string>(maxLength: 50, nullable: false),
                    FatherPhone = table.Column<string>(maxLength: 15, nullable: false),
                    MotherName = table.Column<string>(maxLength: 50, nullable: false),
                    MotherOccupation = table.Column<string>(maxLength: 50, nullable: false),
                    MotherPhone = table.Column<string>(maxLength: 15, nullable: false),
                    FormarSchoolName = table.Column<string>(maxLength: 30, nullable: true),
                    ApplingDate = table.Column<DateTime>(nullable: false),
                    BrunchId = table.Column<int>(nullable: true),
                    SchoolClassId = table.Column<int>(nullable: true),
                    QuotaId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdmissionApply", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdmissionApply_Brunch_BrunchId",
                        column: x => x.BrunchId,
                        principalTable: "Brunch",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdmissionApply_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdmissionApply_District_DistrictId",
                        column: x => x.DistrictId,
                        principalTable: "District",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdmissionApply_Division_DivisionId",
                        column: x => x.DivisionId,
                        principalTable: "Division",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdmissionApply_PoliceStation_PoliceStationId",
                        column: x => x.PoliceStationId,
                        principalTable: "PoliceStation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdmissionApply_PostOffice_PostOfficeId",
                        column: x => x.PostOfficeId,
                        principalTable: "PostOffice",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdmissionApply_Quota_QuotaId",
                        column: x => x.QuotaId,
                        principalTable: "Quota",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdmissionApply_SchoolClass_SchoolClassId",
                        column: x => x.SchoolClassId,
                        principalTable: "SchoolClass",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Guardian",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(maxLength: 100, nullable: false),
                    LastName = table.Column<string>(maxLength: 100, nullable: false),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    PasswordHint = table.Column<string>(maxLength: 12, nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    Religion = table.Column<int>(nullable: false),
                    PhoneNo = table.Column<string>(maxLength: 15, nullable: false),
                    Email = table.Column<string>(nullable: false),
                    NidNo = table.Column<string>(maxLength: 30, nullable: false),
                    ImageUrl = table.Column<string>(nullable: true),
                    CurrentStatus = table.Column<string>(maxLength: 500, nullable: true),
                    PresentAddress = table.Column<string>(maxLength: 500, nullable: false),
                    ParmanentAddress = table.Column<string>(maxLength: 500, nullable: false),
                    CountryId = table.Column<int>(nullable: true),
                    DistrictId = table.Column<int>(nullable: true),
                    DivisionId = table.Column<int>(nullable: true),
                    PoliceStationId = table.Column<int>(nullable: true),
                    PostOfficeId = table.Column<int>(nullable: true),
                    AltGuardianName = table.Column<string>(maxLength: 100, nullable: false),
                    RelationOfAltGuardian = table.Column<string>(maxLength: 100, nullable: false),
                    AltGuardianPhoneNo = table.Column<string>(nullable: false),
                    AltGuardianEmail = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Guardian", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Guardian_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Guardian_District_DistrictId",
                        column: x => x.DistrictId,
                        principalTable: "District",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Guardian_Division_DivisionId",
                        column: x => x.DivisionId,
                        principalTable: "Division",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Guardian_PoliceStation_PoliceStationId",
                        column: x => x.PoliceStationId,
                        principalTable: "PoliceStation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Guardian_PostOffice_PostOfficeId",
                        column: x => x.PostOfficeId,
                        principalTable: "PostOffice",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Staff",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(maxLength: 100, nullable: false),
                    LastName = table.Column<string>(maxLength: 100, nullable: false),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    PasswordHint = table.Column<string>(maxLength: 12, nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    Religion = table.Column<int>(nullable: false),
                    PhoneNo = table.Column<string>(maxLength: 15, nullable: false),
                    Email = table.Column<string>(nullable: false),
                    NidNo = table.Column<string>(maxLength: 30, nullable: false),
                    ImageUrl = table.Column<string>(nullable: true),
                    CurrentStatus = table.Column<string>(maxLength: 500, nullable: true),
                    PresentAddress = table.Column<string>(maxLength: 500, nullable: false),
                    ParmanentAddress = table.Column<string>(maxLength: 500, nullable: false),
                    CountryId = table.Column<int>(nullable: true),
                    DistrictId = table.Column<int>(nullable: true),
                    DivisionId = table.Column<int>(nullable: true),
                    PoliceStationId = table.Column<int>(nullable: true),
                    PostOfficeId = table.Column<int>(nullable: true),
                    FathersName = table.Column<string>(maxLength: 100, nullable: false),
                    MothersName = table.Column<string>(maxLength: 100, nullable: false),
                    joiningDate = table.Column<DateTime>(nullable: false),
                    ResignDate = table.Column<DateTime>(nullable: true),
                    BrunchId = table.Column<int>(nullable: true),
                    DesignationId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Staff", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Staff_Brunch_BrunchId",
                        column: x => x.BrunchId,
                        principalTable: "Brunch",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Staff_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Staff_Designation_DesignationId",
                        column: x => x.DesignationId,
                        principalTable: "Designation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Staff_District_DistrictId",
                        column: x => x.DistrictId,
                        principalTable: "District",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Staff_Division_DivisionId",
                        column: x => x.DivisionId,
                        principalTable: "Division",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Staff_PoliceStation_PoliceStationId",
                        column: x => x.PoliceStationId,
                        principalTable: "PoliceStation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Staff_PostOffice_PostOfficeId",
                        column: x => x.PostOfficeId,
                        principalTable: "PostOffice",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Teacher",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(maxLength: 100, nullable: false),
                    LastName = table.Column<string>(maxLength: 100, nullable: false),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    PasswordHint = table.Column<string>(maxLength: 12, nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    Religion = table.Column<int>(nullable: false),
                    PhoneNo = table.Column<string>(maxLength: 15, nullable: false),
                    Email = table.Column<string>(nullable: false),
                    NidNo = table.Column<string>(maxLength: 30, nullable: false),
                    ImageUrl = table.Column<string>(nullable: true),
                    CurrentStatus = table.Column<string>(maxLength: 500, nullable: true),
                    PresentAddress = table.Column<string>(maxLength: 500, nullable: false),
                    ParmanentAddress = table.Column<string>(maxLength: 500, nullable: false),
                    CountryId = table.Column<int>(nullable: true),
                    DistrictId = table.Column<int>(nullable: true),
                    DivisionId = table.Column<int>(nullable: true),
                    PoliceStationId = table.Column<int>(nullable: true),
                    PostOfficeId = table.Column<int>(nullable: true),
                    MarritialStatus = table.Column<bool>(nullable: false),
                    FathersName = table.Column<string>(maxLength: 100, nullable: false),
                    MothersName = table.Column<string>(maxLength: 100, nullable: false),
                    joiningDate = table.Column<DateTime>(nullable: false),
                    ResignDate = table.Column<DateTime>(nullable: true),
                    BrunchId = table.Column<int>(nullable: true),
                    DesignationId = table.Column<int>(nullable: true),
                    SubjectId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teacher", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Teacher_Brunch_BrunchId",
                        column: x => x.BrunchId,
                        principalTable: "Brunch",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Teacher_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Teacher_Designation_DesignationId",
                        column: x => x.DesignationId,
                        principalTable: "Designation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Teacher_District_DistrictId",
                        column: x => x.DistrictId,
                        principalTable: "District",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Teacher_Division_DivisionId",
                        column: x => x.DivisionId,
                        principalTable: "Division",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Teacher_PoliceStation_PoliceStationId",
                        column: x => x.PoliceStationId,
                        principalTable: "PoliceStation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Teacher_PostOffice_PostOfficeId",
                        column: x => x.PostOfficeId,
                        principalTable: "PostOffice",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Teacher_Subject_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subject",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Students",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(maxLength: 100, nullable: false),
                    LastName = table.Column<string>(maxLength: 100, nullable: false),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    PasswordHint = table.Column<string>(maxLength: 12, nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    Religion = table.Column<int>(nullable: false),
                    PhoneNo = table.Column<string>(maxLength: 15, nullable: false),
                    Email = table.Column<string>(nullable: false),
                    NidNo = table.Column<string>(maxLength: 30, nullable: false),
                    ImageUrl = table.Column<string>(nullable: true),
                    CurrentStatus = table.Column<string>(maxLength: 500, nullable: true),
                    PresentAddress = table.Column<string>(maxLength: 500, nullable: false),
                    ParmanentAddress = table.Column<string>(maxLength: 500, nullable: false),
                    CountryId = table.Column<int>(nullable: true),
                    DistrictId = table.Column<int>(nullable: true),
                    DivisionId = table.Column<int>(nullable: true),
                    PoliceStationId = table.Column<int>(nullable: true),
                    PostOfficeId = table.Column<int>(nullable: true),
                    RegistrationNo = table.Column<int>(nullable: false),
                    RollNo = table.Column<int>(nullable: false),
                    FatherName = table.Column<string>(maxLength: 100, nullable: false),
                    FatherOccupation = table.Column<string>(maxLength: 100, nullable: false),
                    FatherPhoneNo = table.Column<string>(maxLength: 15, nullable: false),
                    MotherName = table.Column<string>(maxLength: 100, nullable: false),
                    MotherOccupation = table.Column<string>(maxLength: 100, nullable: false),
                    MotherPhone = table.Column<string>(maxLength: 15, nullable: false),
                    FormarSchoolName = table.Column<string>(nullable: true),
                    AdmissionDate = table.Column<DateTime>(nullable: false),
                    BrunchId = table.Column<int>(nullable: true),
                    SchoolClassId = table.Column<int>(nullable: true),
                    QuotaId = table.Column<int>(nullable: true),
                    GuardianId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Students", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Students_Brunch_BrunchId",
                        column: x => x.BrunchId,
                        principalTable: "Brunch",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Students_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Students_District_DistrictId",
                        column: x => x.DistrictId,
                        principalTable: "District",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Students_Division_DivisionId",
                        column: x => x.DivisionId,
                        principalTable: "Division",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Students_Guardian_GuardianId",
                        column: x => x.GuardianId,
                        principalTable: "Guardian",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Students_PoliceStation_PoliceStationId",
                        column: x => x.PoliceStationId,
                        principalTable: "PoliceStation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Students_PostOffice_PostOfficeId",
                        column: x => x.PostOfficeId,
                        principalTable: "PostOffice",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Students_Quota_QuotaId",
                        column: x => x.QuotaId,
                        principalTable: "Quota",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Students_SchoolClass_SchoolClassId",
                        column: x => x.SchoolClassId,
                        principalTable: "SchoolClass",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClassRoutine",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DayOfWeek = table.Column<int>(nullable: false),
                    StartTime = table.Column<TimeSpan>(nullable: false),
                    EndTime = table.Column<TimeSpan>(nullable: false),
                    ClassDueation = table.Column<string>(maxLength: 100, nullable: false),
                    PeriodNumber = table.Column<int>(nullable: false),
                    SubjectId = table.Column<int>(nullable: true),
                    TeacherId = table.Column<int>(nullable: true),
                    ClassRoomId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassRoutine", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClassRoutine_ClassRoom_ClassRoomId",
                        column: x => x.ClassRoomId,
                        principalTable: "ClassRoom",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClassRoutine_Subject_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subject",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClassRoutine_Teacher_TeacherId",
                        column: x => x.TeacherId,
                        principalTable: "Teacher",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ExamResult",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ExamPublishDate = table.Column<DateTime>(nullable: false),
                    TotalScore = table.Column<int>(nullable: false),
                    PassScore = table.Column<int>(nullable: false),
                    ObtainScore = table.Column<int>(nullable: false),
                    HighScore = table.Column<int>(nullable: false),
                    GrandTotalScore = table.Column<int>(nullable: false),
                    LatterGrade = table.Column<string>(maxLength: 10, nullable: false),
                    GradePoint = table.Column<double>(nullable: false),
                    Position = table.Column<int>(nullable: false),
                    ResultStatus = table.Column<bool>(nullable: false),
                    TotalPresent = table.Column<int>(nullable: false),
                    TotalAbsent = table.Column<int>(nullable: false),
                    Note = table.Column<string>(maxLength: 400, nullable: true),
                    SchoolClassId = table.Column<int>(nullable: true),
                    ExamId = table.Column<int>(nullable: true),
                    StudentId = table.Column<int>(nullable: true),
                    SubjectId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExamResult", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExamResult_Exam_ExamId",
                        column: x => x.ExamId,
                        principalTable: "Exam",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExamResult_SchoolClass_SchoolClassId",
                        column: x => x.SchoolClassId,
                        principalTable: "SchoolClass",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExamResult_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExamResult_Subject_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subject",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionApply_BrunchId",
                table: "AdmissionApply",
                column: "BrunchId");

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionApply_CountryId",
                table: "AdmissionApply",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionApply_DistrictId",
                table: "AdmissionApply",
                column: "DistrictId");

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionApply_DivisionId",
                table: "AdmissionApply",
                column: "DivisionId");

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionApply_PoliceStationId",
                table: "AdmissionApply",
                column: "PoliceStationId");

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionApply_PostOfficeId",
                table: "AdmissionApply",
                column: "PostOfficeId");

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionApply_QuotaId",
                table: "AdmissionApply",
                column: "QuotaId");

            migrationBuilder.CreateIndex(
                name: "IX_AdmissionApply_SchoolClassId",
                table: "AdmissionApply",
                column: "SchoolClassId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_ClassRoom_RoomId",
                table: "ClassRoom",
                column: "RoomId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassRoom_SchoolClassId",
                table: "ClassRoom",
                column: "SchoolClassId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassRoom_SectionId",
                table: "ClassRoom",
                column: "SectionId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassRoom_ShiftId",
                table: "ClassRoom",
                column: "ShiftId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassRoutine_ClassRoomId",
                table: "ClassRoutine",
                column: "ClassRoomId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassRoutine_SubjectId",
                table: "ClassRoutine",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassRoutine_TeacherId",
                table: "ClassRoutine",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_District_DivisionId",
                table: "District",
                column: "DivisionId");

            migrationBuilder.CreateIndex(
                name: "IX_Division_CountryId",
                table: "Division",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_EducationSystem_BrunchId",
                table: "EducationSystem",
                column: "BrunchId");

            migrationBuilder.CreateIndex(
                name: "IX_EducationSystem_SchoolVersionId",
                table: "EducationSystem",
                column: "SchoolVersionId");

            migrationBuilder.CreateIndex(
                name: "IX_EducationSystem_ShiftId",
                table: "EducationSystem",
                column: "ShiftId");

            migrationBuilder.CreateIndex(
                name: "IX_Event_BrunchId",
                table: "Event",
                column: "BrunchId");

            migrationBuilder.CreateIndex(
                name: "IX_Exam_BrunchId",
                table: "Exam",
                column: "BrunchId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamResult_ExamId",
                table: "ExamResult",
                column: "ExamId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamResult_SchoolClassId",
                table: "ExamResult",
                column: "SchoolClassId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamResult_StudentId",
                table: "ExamResult",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamResult_SubjectId",
                table: "ExamResult",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamRoutine_ExamId",
                table: "ExamRoutine",
                column: "ExamId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamRoutine_SchoolClassId",
                table: "ExamRoutine",
                column: "SchoolClassId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamRoutine_ShiftId",
                table: "ExamRoutine",
                column: "ShiftId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamRoutine_SubjectId",
                table: "ExamRoutine",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Guardian_CountryId",
                table: "Guardian",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Guardian_DistrictId",
                table: "Guardian",
                column: "DistrictId");

            migrationBuilder.CreateIndex(
                name: "IX_Guardian_DivisionId",
                table: "Guardian",
                column: "DivisionId");

            migrationBuilder.CreateIndex(
                name: "IX_Guardian_PoliceStationId",
                table: "Guardian",
                column: "PoliceStationId");

            migrationBuilder.CreateIndex(
                name: "IX_Guardian_PostOfficeId",
                table: "Guardian",
                column: "PostOfficeId");

            migrationBuilder.CreateIndex(
                name: "IX_Holiday_BrunchId",
                table: "Holiday",
                column: "BrunchId");

            migrationBuilder.CreateIndex(
                name: "IX_NoticeBoard_BrunchId",
                table: "NoticeBoard",
                column: "BrunchId");

            migrationBuilder.CreateIndex(
                name: "IX_NoticeBoard_SchoolClassId",
                table: "NoticeBoard",
                column: "SchoolClassId");

            migrationBuilder.CreateIndex(
                name: "IX_PoliceStation_DistrictId",
                table: "PoliceStation",
                column: "DistrictId");

            migrationBuilder.CreateIndex(
                name: "IX_PostOffice_PoliceStationId",
                table: "PostOffice",
                column: "PoliceStationId");

            migrationBuilder.CreateIndex(
                name: "IX_Room_BrunchId",
                table: "Room",
                column: "BrunchId");

            migrationBuilder.CreateIndex(
                name: "IX_RulesRegulations_BrunchId",
                table: "RulesRegulations",
                column: "BrunchId");

            migrationBuilder.CreateIndex(
                name: "IX_Section_SchoolClassId",
                table: "Section",
                column: "SchoolClassId");

            migrationBuilder.CreateIndex(
                name: "IX_Section_SubjectGroupId",
                table: "Section",
                column: "SubjectGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Staff_BrunchId",
                table: "Staff",
                column: "BrunchId");

            migrationBuilder.CreateIndex(
                name: "IX_Staff_CountryId",
                table: "Staff",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Staff_DesignationId",
                table: "Staff",
                column: "DesignationId");

            migrationBuilder.CreateIndex(
                name: "IX_Staff_DistrictId",
                table: "Staff",
                column: "DistrictId");

            migrationBuilder.CreateIndex(
                name: "IX_Staff_DivisionId",
                table: "Staff",
                column: "DivisionId");

            migrationBuilder.CreateIndex(
                name: "IX_Staff_PoliceStationId",
                table: "Staff",
                column: "PoliceStationId");

            migrationBuilder.CreateIndex(
                name: "IX_Staff_PostOfficeId",
                table: "Staff",
                column: "PostOfficeId");

            migrationBuilder.CreateIndex(
                name: "IX_Students_BrunchId",
                table: "Students",
                column: "BrunchId");

            migrationBuilder.CreateIndex(
                name: "IX_Students_CountryId",
                table: "Students",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Students_DistrictId",
                table: "Students",
                column: "DistrictId");

            migrationBuilder.CreateIndex(
                name: "IX_Students_DivisionId",
                table: "Students",
                column: "DivisionId");

            migrationBuilder.CreateIndex(
                name: "IX_Students_GuardianId",
                table: "Students",
                column: "GuardianId");

            migrationBuilder.CreateIndex(
                name: "IX_Students_PoliceStationId",
                table: "Students",
                column: "PoliceStationId");

            migrationBuilder.CreateIndex(
                name: "IX_Students_PostOfficeId",
                table: "Students",
                column: "PostOfficeId");

            migrationBuilder.CreateIndex(
                name: "IX_Students_QuotaId",
                table: "Students",
                column: "QuotaId");

            migrationBuilder.CreateIndex(
                name: "IX_Students_SchoolClassId",
                table: "Students",
                column: "SchoolClassId");

            migrationBuilder.CreateIndex(
                name: "IX_Subject_SubjectGroupId",
                table: "Subject",
                column: "SubjectGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Teacher_BrunchId",
                table: "Teacher",
                column: "BrunchId");

            migrationBuilder.CreateIndex(
                name: "IX_Teacher_CountryId",
                table: "Teacher",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Teacher_DesignationId",
                table: "Teacher",
                column: "DesignationId");

            migrationBuilder.CreateIndex(
                name: "IX_Teacher_DistrictId",
                table: "Teacher",
                column: "DistrictId");

            migrationBuilder.CreateIndex(
                name: "IX_Teacher_DivisionId",
                table: "Teacher",
                column: "DivisionId");

            migrationBuilder.CreateIndex(
                name: "IX_Teacher_PoliceStationId",
                table: "Teacher",
                column: "PoliceStationId");

            migrationBuilder.CreateIndex(
                name: "IX_Teacher_PostOfficeId",
                table: "Teacher",
                column: "PostOfficeId");

            migrationBuilder.CreateIndex(
                name: "IX_Teacher_SubjectId",
                table: "Teacher",
                column: "SubjectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdmissionApply");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "ClassRoutine");

            migrationBuilder.DropTable(
                name: "EducationSystem");

            migrationBuilder.DropTable(
                name: "Event");

            migrationBuilder.DropTable(
                name: "ExamResult");

            migrationBuilder.DropTable(
                name: "ExamRoutine");

            migrationBuilder.DropTable(
                name: "Holiday");

            migrationBuilder.DropTable(
                name: "NoticeBoard");

            migrationBuilder.DropTable(
                name: "RulesRegulations");

            migrationBuilder.DropTable(
                name: "Staff");

            migrationBuilder.DropTable(
                name: "Worker");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "ClassRoom");

            migrationBuilder.DropTable(
                name: "Teacher");

            migrationBuilder.DropTable(
                name: "SchoolVersion");

            migrationBuilder.DropTable(
                name: "Students");

            migrationBuilder.DropTable(
                name: "Exam");

            migrationBuilder.DropTable(
                name: "Room");

            migrationBuilder.DropTable(
                name: "Section");

            migrationBuilder.DropTable(
                name: "Shift");

            migrationBuilder.DropTable(
                name: "Designation");

            migrationBuilder.DropTable(
                name: "Subject");

            migrationBuilder.DropTable(
                name: "Guardian");

            migrationBuilder.DropTable(
                name: "Quota");

            migrationBuilder.DropTable(
                name: "Brunch");

            migrationBuilder.DropTable(
                name: "SchoolClass");

            migrationBuilder.DropTable(
                name: "SubjectGroup");

            migrationBuilder.DropTable(
                name: "PostOffice");

            migrationBuilder.DropTable(
                name: "PoliceStation");

            migrationBuilder.DropTable(
                name: "District");

            migrationBuilder.DropTable(
                name: "Division");

            migrationBuilder.DropTable(
                name: "Country");
        }
    }
}
