﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClassRoomController : ControllerBase
    {
        IClassRoomRepository _classRoomRepository;
        public ClassRoomController(IClassRoomRepository classRoomRepository)
        {
            _classRoomRepository = classRoomRepository;
        }

        // GET: api/ClassRoom
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ClassRoom>>> GetClassRooms()
        {
            try
            {
                var classRooms = await _classRoomRepository.GetClassRooms();
                if (classRooms == null)
                {
                    return NotFound();
                }

                return Ok(classRooms);
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }

        // GET: api/ClassRoom/3
        [HttpGet("{id}")]
        public async Task<ActionResult<ClassRoom>> GetClassRoom(int id)
        {
            var classRoom = await _classRoomRepository.GetClassRoom(id);

            if (classRoom == null)
            {
                return NotFound();
            }

            return classRoom;
        }

        // POST: api/ClassRoom
        [HttpPost]
        public async Task<ActionResult<ClassRoom>> CreateClassRoom(ClassRoom classRoom)
        {
            if (ModelState.IsValid)
            {
                await _classRoomRepository.CreateClassRoom(classRoom);
                return CreatedAtAction("GetClassRooms", new { id = classRoom.Id }, classRoom);
            }
            return BadRequest("Model is not valid");
        }

        // PUT: api/ClassRoom/5
        [HttpPut("{id}")]
        public async Task<ActionResult<ClassRoom>> UpdateClassRoom(int id, ClassRoom classRoom)
        {
            if (id != classRoom.Id)
            {
                return BadRequest();
            }
            var clr = await _classRoomRepository.UpdateClassRoom(id, classRoom);
            return Ok(clr);
        }

        // DELETE: api/ClassRoom/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ClassRoom>> DeleteClassRoom(int id)
        {
            try
            {
                await _classRoomRepository.DeleteClassRoom(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
