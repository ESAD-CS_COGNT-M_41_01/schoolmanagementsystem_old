﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubjectsController : ControllerBase
    {
        private readonly ISubjectRepository _context;

        public SubjectsController(ISubjectRepository context)
        {
            _context = context;
        }

        // GET: api/Subjects
        [HttpGet]
        public ActionResult<IEnumerable<Subject>> GetSubject()
        {
            return _context.GetAll().ToList();
        }

        // GET: api/Subjects/5
        [HttpGet("{id}")]
        public ActionResult<Subject> GetSubject(int id)
        {
            var subject = _context.Find(id);

            if (subject == null)
            {
                return NotFound();
            }

            return subject;
        }

        // PUT: api/Subjects/5

        [HttpPut("{id}")]
        public async Task<IActionResult> PutSubject(int id, Subject subject)
        {
            if (id != subject.Id)
            {
                return BadRequest();
            }

            var s = await _context.SaveSubject(subject);
            return Ok(s);
        }

        // POST: api/Subjects

        [HttpPost]
        public async Task<ActionResult<Subject>> PostSubject(Subject subject)
        {
            if (ModelState.IsValid)
            {
                await _context.SaveSubject(subject);
                return CreatedAtAction("GetSubject", new { id = subject.Id }, subject);
            }
            return BadRequest("Model is not valid");


        }

        // DELETE: api/Subjects/5
        [HttpDelete("{id}")]
        public ActionResult<Subject> DeleteSubject(int id)
        {
            try
            {
                _context.Remove(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
