﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubjectGroupsController : ControllerBase
    {
        private readonly ISubjectGroupRepository _context;

        public SubjectGroupsController(ISubjectGroupRepository context)
        {
            _context = context;
        }

        // GET: api/SubjectGroups
        [HttpGet]
        public ActionResult<IEnumerable<SubjectGroup>> GetSubjectGroups()
        {
            return _context.GetAll().ToList();
        }

        // GET: api/SubjectGroups/5
        [HttpGet("{id}")]

        public ActionResult<SubjectGroup> GetSubjectGroups(int id)
        {
            var SubjectGroup = _context.Find(id);

            if (SubjectGroup == null)
            {
                return NotFound();
            }

            return SubjectGroup;
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutSubjectGroup(int id, SubjectGroup subjectGroup)
        {
            if (id != subjectGroup.Id)
            {
                return BadRequest();
            }
            var sg = await _context.SaveSubjectGroup(subjectGroup);
            return Ok(sg);


        }

        // POST: api/SubjectGroups

        [HttpPost]
        public async Task<ActionResult<SubjectGroup>> PostSubjectGroup(SubjectGroup subjectGroup)
        {
            if (ModelState.IsValid)
            {
                await _context.SaveSubjectGroup(subjectGroup);
                return CreatedAtAction("GetSubjectGroup", new { id = subjectGroup.Id }, subjectGroup);
            }
            return BadRequest("Model is not valid");
        }

        //DELETE: api/SubjectGroups/5

        [HttpDelete("{id}")]
        public ActionResult<SubjectGroup> DeleteSubjectGroup(int id)
        {
            try
            {
                _context.Remove(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }
    }
}
