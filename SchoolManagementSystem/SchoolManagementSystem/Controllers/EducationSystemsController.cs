﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EducationSystemsController : ControllerBase
    {
        private readonly IEducationSystemRepository _context;

        public EducationSystemsController(IEducationSystemRepository context)
        {
            _context = context;
        }

        // GET: api/EducationSystems
        [HttpGet]
        public  ActionResult<IEnumerable<EducationSystem>> GetEducationSystem()
        {
            return  _context.GetAll().ToList();
        }

        // GET: api/EducationSystems/5
        [HttpGet("{id}")]
        public ActionResult<EducationSystem> GetEducationSystem(int id)
        {
            var educationSystem =  _context.Find(id);

            if (educationSystem == null)
            {
                return NotFound();
            }

            return educationSystem;
        }

        // PUT: api/EducationSystems/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEducationSystem(int id, EducationSystem educationSystem)
        {
            if (id != educationSystem.Id)
            {
                return BadRequest();
            }                     
            var res = await _context.SaveEducationSystem(educationSystem);            
            return Ok(res);
        }

        // POST: api/EducationSystems
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<EducationSystem>> PostEducationSystem(EducationSystem educationSystem)
        {
            if (ModelState.IsValid)
            {
                await _context.SaveEducationSystem(educationSystem);
                return CreatedAtAction("GetEducationSystem", new { id = educationSystem.Id }, educationSystem);
            }
            return BadRequest("Model is not valid");
        }

        // DELETE: api/EducationSystems/5
        [HttpDelete("{id}")]
        public ActionResult<EducationSystem> DeleteEducationSystem(int id)
        {
            try
            {
                _context.Remove(id);
                return Ok();
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
