﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BrunchesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public BrunchesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Brunches
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Brunch>>> GetBrunch()
        {
            return await _context.Brunch.ToListAsync();
        }

        // GET: api/Brunches/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Brunch>> GetBrunch(int id)
        {
            var brunch = await _context.Brunch.FindAsync(id);

            if (brunch == null)
            {
                return NotFound();
            }

            return brunch;
        }

        // PUT: api/Brunches/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBrunch(int id, Brunch brunch)
        {
            if (id != brunch.Id)
            {
                return BadRequest();
            }

            _context.Entry(brunch).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BrunchExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Brunches
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Brunch>> PostBrunch(Brunch brunch)
        {
            _context.Brunch.Add(brunch);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBrunch", new { id = brunch.Id }, brunch);
        }

        // DELETE: api/Brunches/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Brunch>> DeleteBrunch(int id)
        {
            var brunch = await _context.Brunch.FindAsync(id);
            if (brunch == null)
            {
                return NotFound();
            }

            _context.Brunch.Remove(brunch);
            await _context.SaveChangesAsync();

            return brunch;
        }

        private bool BrunchExists(int id)
        {
            return _context.Brunch.Any(e => e.Id == id);
        }
    }
}
