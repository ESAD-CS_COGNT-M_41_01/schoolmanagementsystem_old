﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SchoolManagementSystem.Identity;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WorkersController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        //private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _rollManager;
   
        
        public WorkersController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> rollManager /*SignInManager<ApplicationUser> signInManager*/)
        {
            _context = context;
            //_signInManager = signInManager;
            _userManager = userManager;
            _rollManager = rollManager;
        }

        // GET: api/Workers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Worker>>> GetWorker()
        {
            return await _context.Worker.ToListAsync();
        }

        // GET: api/Workers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Worker>> GetWorker(int id)
        {
            var worker = await _context.Worker.FindAsync(id);

            if (worker == null)
            {
                return NotFound();
            }

            return worker;
        }

        // PUT: api/Workers/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutWorker(int id, Worker worker)
        {
            if (id != worker.Id)
            {
                return BadRequest();
            }
            var oldWorker = _context.Worker.Where(w => w.Id == worker.Id).FirstOrDefault();
            ApplicationUser userWorker = _context.Users.Where(w => w.Email == oldWorker.Email).FirstOrDefault();
            if(userWorker != null) {
                userWorker.Email = worker.Email;
                userWorker.UserName = worker.FullName;
            };
            IdentityResult result = await _userManager.UpdateAsync(userWorker);

            if (result.Succeeded)
            {
                var group = _context.Worker.First(g => g.Id == worker.Id);
                _context.Entry(group).CurrentValues.SetValues(worker);
                await _context.SaveChangesAsync();
                //_context.Update(worker);
                //_context.Entry(worker).State = EntityState.Modified;
                try
                {
                    await _context.SaveChangesAsync();
                    return Ok();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WorkerExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }                     
            return BadRequest();
        }

        // POST: api/Workers
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        //[Route("api/[controller]/[action]")]
        public async Task<ActionResult<Worker>> PostWorker(Worker worker)
        {
            ApplicationUser user = new ApplicationUser()
            {
                Email = worker.Email,
                UserName = worker.FullName

            };
            IdentityResult result = await _userManager.CreateAsync(user, worker.FullName);

            if (!result.Succeeded)
            {
                return BadRequest(result.Errors);
            }
            _context.Worker.Add(worker);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetWorker", new { id = worker.Id }, worker);
        }

        // DELETE: api/Workers/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Worker>> DeleteWorker(int id)
        {
            try
            {
                var worker = await _context.Worker.FindAsync(id);

                if (worker == null)
                {
                    return NotFound();
                }

                ApplicationUser userWorker = _context.Users.Where(w => w.Email == worker.Email).FirstOrDefault();
                IdentityResult result = await _userManager.DeleteAsync(userWorker);

                if (result.Succeeded)
                {
                    var group = _context.Worker.First(g => g.Id == worker.Id);

                    _context.Worker.Remove(worker);
                    await _context.SaveChangesAsync();

                    return worker;

                }
            }
            catch(Exception ex)
            {
               return BadRequest(ex.Message);
            }
            
            return BadRequest("Not found your Id");
        }

        private bool WorkerExists(int id)
        {
            return _context.Worker.Any(e => e.Id == id);
        }

        // POST: api/Workers
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost("{roleName}")]
        public async Task<ActionResult<Worker>> PostWorkerWithRole(Worker worker, string roleName)
        {
            try
            {
                ApplicationUser user = new ApplicationUser()
                {
                    Email = worker.Email,
                    UserName = worker.FullName

                };
                IdentityResult result = await _userManager.CreateAsync(user, worker.FullName);

                if (!result.Succeeded)
                {
                    return BadRequest(result.Errors);
                }
                _context.Worker.Add(worker);
                await _context.SaveChangesAsync();

                var role = await _rollManager.FindByNameAsync(roleName);
                var newuser = await _userManager.FindByEmailAsync(user.Email);

                var roleResult = await _userManager.AddToRoleAsync(newuser, role.Name);

                if (roleResult.Succeeded)
                {
                    return Ok(user.UserName + " is Assigned to " + role.Name + " Role");
                }
  
                //return CreatedAtAction("GetWorker", new { id = worker.Id }, worker);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return BadRequest();
        }
    }
}
