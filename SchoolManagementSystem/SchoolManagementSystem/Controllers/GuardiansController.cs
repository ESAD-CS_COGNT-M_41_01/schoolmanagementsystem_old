﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SchoolManagementSystem.Model;

namespace SchoolManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GuardiansController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public GuardiansController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Guardians
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Guardian>>> GetGuardian()
        {
            return await _context.Guardian.ToListAsync();
        }

        // GET: api/Guardians/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Guardian>> GetGuardian(int id)
        {
            var guardian = await _context.Guardian.FindAsync(id);

            if (guardian == null)
            {
                return NotFound();
            }

            return guardian;
        }

        // PUT: api/Guardians/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGuardian(int id, Guardian guardian)
        {
            if (id != guardian.Id)
            {
                return BadRequest();
            }

            _context.Entry(guardian).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GuardianExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Guardians
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Guardian>> PostGuardian(Guardian guardian)
        {
            _context.Guardian.Add(guardian);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetGuardian", new { id = guardian.Id }, guardian);
        }

        // DELETE: api/Guardians/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Guardian>> DeleteGuardian(int id)
        {
            var guardian = await _context.Guardian.FindAsync(id);
            if (guardian == null)
            {
                return NotFound();
            }

            _context.Guardian.Remove(guardian);
            await _context.SaveChangesAsync();

            return guardian;
        }

        private bool GuardianExists(int id)
        {
            return _context.Guardian.Any(e => e.Id == id);
        }
    }
}
